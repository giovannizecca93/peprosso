<x-layout>

<!-- Header -->
  <header class="container-fluid m-0 p-0">
    <div class="row">
      <div class="col-12 col-lg-8 m-0 p-0" data-aos="fade-right">
        <img class="img-fluid"  src="./media/s1.jpg" alt="immagine peperoncini rossi">
      </div>
      <div class="col-12 col-lg-4 bg-custom" data-aos="fade-left">
        <div class="d-flex flex-column justify-content-center align-items-center text-title-center text-main">
          <h3>Benvenuti su:</h3>
          <h1>Peperoncino Rosso </h1>
          <h3>Le Delizie di Calabria.</h3>
        </div>
      </div>
    </div>
  </header>


  <!--Categorie  -->

  <section class="container my-4 ">

    <h2 class="text-acc text-center my-4">Esplora il nostro blog !</h2>
    <p class="text-center text-acc">Scegli tra le varie categorie e trova la ricetta giusta per te....tutte esclusivamente Made in Calabria.</p>
    
    <div class="hero-section" data-aos="zoom-in-up">
      <div class="card-grid">
        <a class="card" href="#">
          <div class="card__background" style="background-image: url(./media/Anti1.jpg)"></div>
          <div class="card__content">
            <h3 class="card__heading">Antipasti</h3>
          </div>
        </a>
        <a class="card" href="#">
          <div class="card__background" style="background-image: url(./media/11.jpg)"></div>
          <div class="card__content">
            <h3 class="card__heading">Primi</h3>
          </div>
        </a>
        <a class="card" href="#">
          <div class="card__background" style="background-image: url(./media/22.jpg)"></div>
          <div class="card__content">
            <h3 class="card__heading">Secondi</h3>
          </div>
        </a>
        <a class="card" href="#">
          <div class="card__background" style="background-image: url(./media/4.jpg)"></div>
          <div class="card__content">
            <h3 class="card__heading">Contorni</h3>
          </div>
        </a>
        <a class="card" href="#">
          <div class="card__background" style="background-image: url(./media/dolci.jpg)"></div>
          <div class="card__content">
            <h3 class="card__heading">Dolci</h3>
          </div>
        </a>
        <a class="card" href="#">
          <div class="card__background" style="background-image: url(./media/Vini1.jpeg)"></div>
          <div class="card__content">
            <h3 class="card__heading">Vini</h3>
          </div>
        </a>
        
        
      </section>

      <!-- Top Five -->
      
      <section>
        
        <h2 class="text-acc text-center my-4">Top 5 Ricette !</h2>
        <p class="text-center text-acc">Le cinque ricette più cliccate sul nostro blog !</p>
        
      </section>



           <!-- Iscrizione Newsletter  -->
      
      <section>
        
        <h3 class="text-acc text-center my-4">Iscriviti</h3>
        <div>
          <p class="text-acc text-center">Vuoi essere aggiornato sulle ricette che vengono inserite sul nostro blog ??? Iscriviti subito alla nostra newsletter inserendo la tua mail !</p>
        </div>
        <div class="container" data-aos="fade-down"
        data-aos-easing="linear"
        data-aos-duration="1500">
          <div class="row">
            <div class="col-12 subscribe">
              <form action="">
                <div class="row">
                  <div class="col">
                    <input type="text" class="form-control-custom" placeholder="Nome e Cognome" aria-label="Name">
                    <input type="text" class="form-control-custom" placeholder="Indirizzo e-mail" aria-label="e-mail">
                    <button class="btn-custom">Iscriviti</button> 
                  </div>  
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
      















    </x-layout>