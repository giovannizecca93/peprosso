<footer class="row my-5 py-3 border-top bg-dark " data-aos="fade-right"
      data-aos-offset="300"
      data-aos-easing="ease-in-sine">
        <div class="col-12 col-lg-3">
          <a href="/" class="d-flex align-items-center my-5 justify-content-center link-light text-decoration-none text-main">
            <h4>Peperoncino Rosso</h4>   <i class="fa-solid  mx-2 coloricon fa-pepper-hot"></i>
          </a>
          <p class="text-main text-center">© 2021</p>
        </div>
        
        
        <div class="col-12 col-lg-3">
          <h5 class="text-main">Mappa del sito</h5>
          <ul class="nav flex-column">
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Home</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Chi Siamo</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Contatti</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">FAQs</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Iscriviti</a></li>
          </ul>
        </div>
        
        <div class="col-12 col-lg-3">
          <h5 class="text-main">Ricette</h5>
          <ul class="nav flex-column">
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Antipasti</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Primi</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Secondi</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Contorni</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Dolci</a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Vini</a></li>
          </ul>
        </div>
        
        <div class="col-12 col-lg-3">
          <h5 class="text-main">Social</h5>
          <ul class="nav flex-column">
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Facebook <i class="fa-brands me-2 fa-facebook"></i></a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Instagram <i class="fa-brands me-2 fa-instagram"></i></a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Twitter <i class="fa-brands me-2 fa-twitter"></i></a></li>
            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-acc">Pinterest <i class="fa-brands me-2 fa-pinterest"></i></a></li>
          </ul>
        </div>
      </footer>