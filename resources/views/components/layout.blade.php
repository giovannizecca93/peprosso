<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
   
    <!-- AOS -->
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noticia+Text:ital,wght@0,400;0,700;1,400;1,700&family=Righteous&display=swap"        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
   
   
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

</head>
<body>
<x-navbar></x-navbar>


{{$slot}}


<x-topbtn></x-topbtn>



<x-footer></x-footer>
<!-- Fontawesome -->
  <script src="https://kit.fontawesome.com/72938b397d.js" crossorigin="anonymous"></script>

<!-- Aos -->
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
      
<!-- My Js -->
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>