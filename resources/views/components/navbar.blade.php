<nav class="navbar navbar-expand-lg navbar-light bg-dark nav-query sticky-top">
    <div class="container-fluid ">
      <a class="navbar-brand " href="#">Peperoncino Rosso <i class="fa-solid  mx-2 coloricon fa-pepper-hot"></i></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
        <ul class="navbar-nav ">
          <li class="nav-item">
            <a class="nav-link active a-nav" aria-current="page" href="#">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link a-nav" href="#">Chi Siamo</a>
          </li>			
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle a-nav" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Ricette
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="#">Antipasti</a></li>
              <li><a class="dropdown-item" href="#">Primi</a></li>
              <li><a class="dropdown-item" href="#">Secondi</a></li>
              <li><a class="dropdown-item" href="#">Contorni</a></li>         
              <li><a class="dropdown-item" href="#">Pasticceria</a></li>
              <li><a class="dropdown-item" href="#">Vini</a></li>
            </ul>
          </li>
        </ul>	
        <form class="d-flex">
          <input class="form-control me-2" type="search" placeholder="Cerca" aria-label="Search">
          <button class="btn btn-outline-danger" type="submit">Cerca</button>
        </form>	  
      </div>
    </div>
  </nav>